  <!DOCTYPE html>
  <html>
    <head>
      <!--Import Google Icon Font-->
      <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
      <!--Import materialize.css-->
      <link type="text/css" rel="stylesheet" href="materialize/css/materialize.min.css"  media="screen,projection"/>

      <!--Let browser know website is optimized for mobile-->
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
      <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
      <title>MCSP 친선 축구 기록</title>
    </head>

    <body>
      <nav class="light-blue lighten-1" role="navigation">
        <div class="nav-wrapper container"><a id="logo-container" href="#" class="brand-logo">MCSP 친선 축구 기록</a>
          <ul class="right hide-on-med-and-down">
            <li><a href="#">Over view</a></li>
            <li><a href="#">일정/결과</a></li>
            <li><a href="#">기록</a></li>
            <li><a href="#">Members</a></li>
          </ul>

          <a href="#" data-activates="nav-mobile" class="button-collapse"><i class="material-icons">menu</i></a>
          <ul id="nav-mobile" class="side-nav">
            <li><a href="#">Over view</a></li>
            <li><a href="#">일정/결과</a></li>
            <li><a href="#">기록</a></li>
            <li><a href="#">Members</a></li>
          </ul>
          
        </div>
      </nav>

      <div class="container">
        <div class="card-panel teal lighten-4 z-depth-2 col s12 m2">
          <h5>2월 1일 수요일</h5> 13:07~14:48, @연세대학교 대운동장, -2℃/7℃
          <blockquote>
          글빛 광천(GK) 정규 지호 오성 민성 동규 세인(GK) 태현 봉성<br/>
          성목 진영 잭슨(GK) 용석 구형 영환 상근(GK) 민제 진규 채훈 지석
          </blockquote>
        
          7분: 영환 (어시 구형) - 실점 광천<br/>
          51분: 지호 - 실점 잭슨<br/>
          61분: 용석 (어시 구형) - 실점 세인<br/>
          88분: 민성 (어시 동규) - 실점 상근<br/>
          95분: 동규 - 실점 상근
        </div>
      </div>

      <footer class="page-footer orange">
        <div class="container">
          .<br/>
          .<br/>
        </div>
        <div class="footer-copyright">
          <div class="container">
            .
          </div>
        </div>
      </footer>

      <!--Import jQuery before materialize.js-->
      <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
      <script type="text/javascript" src="materialize/js/materialize.min.js"></script>
      <script type="text/javascript">
        $( document ).ready(function() {
          $(".button-collapse").sideNav();
        });
      </script>
    </body>
  </html>